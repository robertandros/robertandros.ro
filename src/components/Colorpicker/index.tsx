import React, { useEffect } from 'react';

import './Colorpicker.scss';
import './Colorpicker.css';


interface Colors {
    [color: string]: ColorProperties
}
interface ColorProperties {
    [property: string]: string
}

const colors: Colors = {
    'cyan': {
        '--primary-font-color': '255,255,255',
        '--secondary-font-color': 'cyan',
        '--background-color': 'black'
    },
    'hotpink': {
        '--primary-font-color': '0,0,0',
        '--secondary-font-color': 'hotpink',
        '--background-color': '#f8f9cd'
    },
    'gold': {
        '--primary-font-color': '255,255,255',
        '--secondary-font-color': 'gold',
        '--background-color': 'black'
    }
}

const Colorpicker = () => {
    const onColorSelect = (color: string) => {
        const properties: ColorProperties = colors[color];

        if(!properties) {
            return;
        }

        localStorage.setItem('color', color);

        Object.keys(properties).forEach((key: string) => {
            const value: string = properties[key];
            document.body.style.setProperty(key, value);
        })
    }

    useEffect(() => {
        onColorSelect(localStorage.getItem('color') || Object.keys(colors)[0]);
    }, []);

    return (
        <div className="colors">
            {Object.keys(colors).map((color: string) => <div onClick={() => onColorSelect(color)} key={color} className="color" style={{backgroundColor: color}} />)}
        </div>
    );
}

export default Colorpicker;
