import React from 'react';
import { Container, Row, Col } from 'react-bootstrap';

import './Section.scss';

const Section = (props: any) => {
  return (
    <Container
      data-aos='fade-up'
      className={`section ${props.hasMargin && 'mt-5'}`}
    >
      <Row>
        <Col>
          <p className='section__title'>{props.title}</p>
        </Col>
      </Row>
      <Row>
        <Col>
          <div className='section__content'>{props.children}</div>
        </Col>
      </Row>
    </Container>
  );
};

export default Section;
