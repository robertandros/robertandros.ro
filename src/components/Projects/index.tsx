import React from 'react';
import { Row, Col } from 'react-bootstrap';
import Section from '../Section';
import Card from '../Card';

import {
  faReact,
  faJs,
  faHtml5,
  faCss3,
  faGit,
} from '@fortawesome/free-brands-svg-icons';

import './Projects.scss';

const Projects = () => {
  return (
    <Section hasMargin title='my projects'>
      <Row>
        <Col>
          <Card
            title='Eat-together'
            description="Project created during <a target='_blank' rel='noopener noreferrer' href='https://fiipractic.asii.ro/'>FII practic.</a><br><br><span>The application's purpose is to help you find great places and friendly people who want to enjoy the next meal together.</span>"
            content={{
              title: 'Technologies',
              list: [
                {
                  icon: faHtml5,
                  alt: 'HTML',
                },
                { icon: faCss3, alt: 'CSS' },
                { icon: faJs, alt: 'Javascript' },
                { icon: faReact, alt: 'React' },
                { icon: faGit, alt: 'Git' },
              ],
            }}
            url='https://gitlab.com/robertandros/fii_practic'
          ></Card>
        </Col>
      </Row>
    </Section>
  );
};

export default Projects;
