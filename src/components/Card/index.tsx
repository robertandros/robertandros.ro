import React from 'react';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';

import './Card.scss';

const Card = (props: any) => {
  return (
    <div className='card'>
      <p className='card__title'>{props.title}</p>

      <p
        className='card__description'
        dangerouslySetInnerHTML={{ __html: props.description }}
      ></p>
      {props.content && (
        <div className='card__technologies'>
          <p className='card__technologies--title'>{props.content.title}</p>
          {props.content.list.map((technology: any, index: number) => (
            <FontAwesomeIcon
              className='card__technology'
              icon={technology.icon}
              title={technology.alt}
              size='2x'
              color='#000'
              key={index}
            />
          ))}
        </div>
      )}
      {props.url && (
        <div className='card__url'>
          <span>Check the code on </span>
          <a
            className='card__url--link'
            target='_blank'
            rel='noopener noreferrer'
            href={props.url}
          >
            Gitlab.
          </a>
        </div>
      )}
    </div>
  );
};

export default Card;
