import React from 'react';
import { Container, Row, Col } from 'react-bootstrap';

import "./Footer.scss";

const Footer = () => {
    return (
        <Container>
            <Row>
                <Col>
                    <div className="footer">
                        <span className="footer__text">Made from scratch with</span>
                        <a className="footer__text--emphasized" href="https://reactjs.org/" rel="noopener noreferrer" target="_blank" > React</a>
                        <span className="footer__text">,</span>
                        <a className="footer__text--emphasized" href="https://getbootstrap.com/" rel="noopener noreferrer" target="_blank" > Bootstrap</a>
                        <span className="footer__text">,</span>
                        <span role="img" aria-label="coffee" className="footer__icon">☕️</span>
                        <span className="footer__text">and</span>
                        <span role="img" aria-label="heart" className="footer__icon">❤️</span>
                        <span className="footer__text">.</span>
                    </div>
                </Col>
            </Row>
        </Container>   
    )
}

export default Footer;
