import React from 'react';
import { Container, Row, Col } from 'react-bootstrap';
import AOS from 'aos';
import Header from '../Header';
import Colorpicker from '../Colorpicker';
import About from '../About';
import Projects from '../Projects';
import Footer from '../Footer';
import Contact from '../Contact';

import './App.scss';
import 'aos/dist/aos.css';

const App = () => {
  AOS.init({
    once: true,
  });

  return (
    <div className='wrapper'>
      <div className='wrapper__content'>
        <Header>
          <Colorpicker />
        </Header>
        <Container>
          <Row>
            <Col>
              <div className='wrapper__separator mt-5 mb-5' />
            </Col>
          </Row>
        </Container>
        <About />
        <Projects />
        <Contact />
      </div>
      <Footer />
    </div>
  );
};

export default App;
