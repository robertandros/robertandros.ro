import React from 'react';
import { Button, Spinner } from 'react-bootstrap';
import mailgun from 'mailgun-js';
import Section from '../Section';

import './Contact.css';

const initialState = {
  name: '',
  email: '',
  message: '',
  isSending: false,
};

class Contact extends React.Component {
  state = {
    ...initialState,
  };

  handleSubmit = (e: React.FormEvent<HTMLFormElement>) => {
    e.preventDefault();

    this.setState({ isSending: true });

    setTimeout(() => {
      this.setState({
        isSending: false,
      });
    }, 2000);
    // const mg = mailgun({
    //   apiKey: process.env.REACT_APP_MAILGUN_API_KEY as string,
    //   domain: process.env.REACT_APP_MAILGUN_DOMAIN as string,
    // });

    // const data: any = {
    //   from: `${this.state.name} <${this.state.email}>`,
    //   to: 'emil.andros@gmail.com',
    //   subject: 'Colaboration',
    //   text: this.state.message,
    // };

    // mg.messages().send(data, (error) => {
    //   if (!error) {
    //     this.setState({ ...initialState });
    //   } else {
    //     alert('An error occured! Please try again later.');
    //   }
    // });
  };

  onNameChange = (event: React.ChangeEvent<HTMLInputElement>) => {
    this.setState({ name: event.target.value });
  };

  onEmailChange = (event: React.ChangeEvent<HTMLInputElement>) => {
    this.setState({ email: event.target.value });
  };

  onMessageChange = (event: React.ChangeEvent<HTMLTextAreaElement>) => {
    this.setState({ message: event.target.value });
  };

  render() {
    return (
      <Section hasMargin title='contact me'>
        <form
          id='contact-form'
          onSubmit={this.handleSubmit}
          className='col-12 col-lg-6'
        >
          <div className='form-group'>
            <label htmlFor='name'>Name</label>
            <input
              type='text'
              className='form-control'
              id='name'
              required
              disabled={this.state.isSending}
              value={this.state.name}
              onChange={this.onNameChange}
            />
          </div>
          <div className='form-group'>
            <label htmlFor='email'>Email address</label>
            <input
              type='email'
              className='form-control'
              id='email'
              required
              disabled={this.state.isSending}
              value={this.state.email}
              onChange={this.onEmailChange}
            />
          </div>
          <div className='form-group'>
            <label htmlFor='message'>Message</label>
            <textarea
              className='form-control'
              rows={5}
              id='message'
              required
              disabled={this.state.isSending}
              value={this.state.message}
              onChange={this.onMessageChange}
            />
          </div>
          <Button disabled={this.state.isSending} type='submit'>
            {this.state.isSending ? 'Sending...' : 'Submit'}
          </Button>
        </form>
      </Section>
    );
  }
}

export default Contact;
