import React from 'react';
import { Container, Row, Col } from 'react-bootstrap';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faGitlab, faLinkedin } from '@fortawesome/free-brands-svg-icons';

import './Header.scss';

export default (props: any) => {
  return (
    <div data-aos='fade-in' className='header'>
      {props.children}
      <Container>
        <Row className='header__line'>
          <Col sm={10} md={9}>
            <span className='header__subtitle'>
              <span className='header__hi' role='img' aria-label='hi'>
                👋
              </span>
              <span>hi, i'm </span>
            </span>
            <span className='header__title'>Robert Andros</span>
            <span className='header__underscore'></span>
          </Col>
        </Row>
        <Row className='header__line header__line--with-margin'>
          <Col className='header__icons' sm={10} md={9}>
            <a className='header__icon' href='https://gitlab.com/robertandros'>
              <FontAwesomeIcon icon={faGitlab} size='2x' />
            </a>
            <a
              className='header__icon'
              href='https://www.linkedin.com/in/robert-andros-a91266132'
            >
              <FontAwesomeIcon icon={faLinkedin} size='2x' />
            </a>
          </Col>
        </Row>
      </Container>
    </div>
  );
};
