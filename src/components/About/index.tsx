import React, { useState, useEffect } from 'react';
import * as moment from 'moment';
import 'moment-duration-format';
import Section from '../Section';

import './About.scss';

const About = () => {
  const START_DATE = Date.parse('01 Jul 2016 10:00:00');

  const getWorkingTime = () => {
    const currentDate: number = new Date().getTime();
    const seconds = Math.floor((currentDate - START_DATE) / 1000);
    return moment
      .duration(seconds, 'seconds')
      .format(
        `y [years], M [months], d [days], h [hours], m [minutes] [and] s [seconds]`
      );
  };

  const [workingTime, setWorkingTime] = useState(getWorkingTime());

  useEffect(() => {
    setInterval(() => {
      const workingTime = getWorkingTime();
      setWorkingTime(workingTime);
    }, 1000);
  }, []);

  return (
    <Section title='some things about me'>
      <ul className='about__list'>
        <li>
          software developer for{' '}
          <span
            title={`since ${new Date(START_DATE).toLocaleDateString()}`}
            className='about__text--emphasized'
          >
            {workingTime}
          </span>
        </li>
        <li>
          javascript maniac &nbsp;
          <span role='img' aria-label='maniac'>
            🧑‍💻
          </span>
        </li>
        <li>
          learning new things{' '}
          <a
            href='https://www.youtube.com/watch?v=_qQsPw4CqUI'
            rel='noopener noreferrer'
            target='_blank'
            className='about__text--emphasized about__text--underlined'
          >
            everyday
          </a>
        </li>
        <li className='about__text--cut'>i don't know how to tell jokes</li>
      </ul>
    </Section>
  );
};

export default About;
