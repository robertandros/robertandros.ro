import React from 'react';
import ReactDOM from 'react-dom';
import App from './components/App';

import './reset.css';
import './variables.css';

ReactDOM.render(<App />, document.getElementById('root'));
